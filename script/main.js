

class HamburgerException  {
    constructor(message){
        this.message = message;}

}

class Hamburger {
    constructor (sizeBurger, stuffingBurger) {
        this._size = sizeBurger
        this._stuffing = stuffingBurger
        this._topping = []
    }
    addTopping = function (toppingBurger) {
        try {
            if (toppingBurger.topping) {
                let checkTopping = this.topping.some((el) => {
                    return el === toppingBurger
                })
                if (!checkTopping) {
                    this.topping.push(toppingBurger)
                } else {
                    console.log('Такой топпинг уже есть в вашем бургере')
                }
            } else {
                throw new HamburgerException("Вы добавляете не топпинг");
            }
        } catch (e) {
            console.error(e);
        }
    }

    get topping () {
        return this._topping;
    }
    set topping (value) {
        try {
            if ( value.topping ) {
                this._topping.forEach((toppingInBurger) => {
                    if (toppingInBurger === value) {
                        const idx = this._topping.indexOf(value)
                        this._topping.splice(idx)
                    } else {
                        console.log('В вашем бургере нет такого топпинга')
                    }
                })
            } else {
                throw new Error('Вы ввели не топпинг')
            }
        } catch (e) {
             console.error(e);
        }
    }

    get size () {
        return  this._size.size
    }
    get stuffing () {
        return  this._stuffing.stuffing
    }
    calculatePrice = function () {
        let currentPrice
        try {
            if ( this._size ) {
                if ( this._stuffing ) {
                    currentPrice = this._size.price
                        + this._stuffing.price
                    if ( this._topping.length ) {
                        this._topping.forEach((el) => {
                            currentPrice += el.price
                        })
                    } else {
                        throw new HamburgerException("Для подсчета цены не хватает топпинга")
                    }
                    console.log(`Price your's burger - ${currentPrice}`)
                } else {
                    throw new HamburgerException("Для подсчета цены не хватает начинки")
                }
            } else {
                throw new HamburgerException("Для подсчета цены не хватает бургера")
            }
        } catch (e) {
            console.log(e.message);
        }
        return currentPrice
    }
    calculateCalories = function () {
        let currentCalories
        try {
            if ( this._size ) {
                if ( this._stuffing ) {
                    currentCalories = this._size.calories
                        + this._stuffing.calories
                    if ( this._topping.length ) {
                        this._topping.forEach((el) => {
                            currentCalories += el.calories
                        })
                    } else {
                        throw new HamburgerException("Для подсчета каллорий не хватает топпинга")
                    }
                    console.log(`Calories in your's burger - ${currentCalories}`)
                } else {
                    throw new HamburgerException("Для подсчета каллорий не хватает начинки")
                }
            } else {
                throw new HamburgerException("Для подсчета каллорий не хватает бургера")
            }
        } catch (e) {
            console.log(e.message);
        }
        return currentCalories
    }

}

Hamburger.SIZE_SMALL = { size : 'small', price : 50, calories : 20 };
Hamburger.SIZE_LARGE = { size : 'large', price : 100, calories : 40 };
Hamburger.STUFFING_CHEESE = { stuffing : 'cheese', price : 10, calories : 20 };
Hamburger.STUFFING_SALAD = {  stuffing : 'salad', price : 20, calories : 5 };
Hamburger.STUFFING_POTATO = { stuffing : 'potato', price : 15, calories : 10 };
Hamburger.TOPPING_MAYO = { topping : 'mayonnaise', price : 20, calories : 5 };
Hamburger.TOPPING_SPICE = { topping : 'spice', price : 15, calories : 0 };

let burger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_POTATO)
// burger.addTopping(Hamburger.SIZE_SMALL)
// burger.addTopping(Hamburger.TOPPING_SPICE)
burger.addTopping(Hamburger.TOPPING_MAYO)
// burger.topping = Hamburger.SIZE_SMALL
// console.log(burger.size)
burger.calculatePrice()
burger.calculateCalories()
console.log(burger)